package clever.test.com.testproject.model;

import android.content.ContentValues;
import android.database.Cursor;

import com.google.api.services.customsearch.model.Result;

import clever.test.com.testproject.provider.FavoritesProvider;
import clever.test.com.testproject.provider.ImagesProvider;

/**
 * Created by user on 20.02.15.
 */
public class FavoriteModelPersistant {

    private int id;
    private String title;
    private String thumbnailLocalPath;
    private String localPath;

    public FavoriteModelPersistant(ImageModelPersistant imageModel, String title) {
        this.title = title;
        this.id = imageModel.getId();
        this.thumbnailLocalPath = imageModel.getThumbnailLocalPath();
        this.localPath = imageModel.getThumbnailLocalPath();
    }

    public FavoriteModelPersistant(Cursor cursor) {
        id = cursor.getInt(cursor.getColumnIndex(FavoritesProvider.ID));
        title = cursor.getString(cursor.getColumnIndex(FavoritesProvider.IMAGE_TITLE));
        thumbnailLocalPath = cursor.getString(cursor.getColumnIndex(FavoritesProvider.IMAGE_THUMBNAIL_LOCAL_PATH));
        localPath = cursor.getString(cursor.getColumnIndex(FavoritesProvider.IMAGE_LOCAL_PATH));
    }

    public ContentValues toContentValues() {
        ContentValues cv = new ContentValues();
        cv.put(FavoritesProvider.ID, id);
        cv.put(FavoritesProvider.IMAGE_TITLE, title);
        cv.put(FavoritesProvider.IMAGE_LOCAL_PATH, localPath);
        cv.put(FavoritesProvider.IMAGE_THUMBNAIL_LOCAL_PATH, thumbnailLocalPath);
        return cv;
    }

    public String getTitle() {
        return title;
    }

    public void setThumbnailLocalPath(String thumbnailLocalPath) {
        this.thumbnailLocalPath = thumbnailLocalPath;
    }

    public String getThumbnailLocalPath() {
        return thumbnailLocalPath;
    }

    public String getLocalPath() {
        return localPath;
    }

}
