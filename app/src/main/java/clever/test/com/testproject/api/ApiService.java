package clever.test.com.testproject.api;

import android.app.IntentService;
import android.content.ContentValues;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.util.Log;

import com.google.api.client.extensions.android.http.AndroidHttp;
import com.google.api.client.http.HttpTransport;
import com.google.api.client.json.JsonFactory;
import com.google.api.client.json.jackson2.JacksonFactory;
import com.google.api.services.customsearch.Customsearch;
import com.google.api.services.customsearch.CustomsearchRequestInitializer;
import com.google.api.services.customsearch.model.Result;
import com.google.api.services.customsearch.model.Search;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.StatusLine;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;

import java.io.FileOutputStream;
import java.io.IOException;
import java.security.GeneralSecurityException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

import clever.test.com.testproject.Const;
import clever.test.com.testproject.model.ImageModelPersistant;
import clever.test.com.testproject.provider.ImagesProvider;

/**
 * Created by user on 19.02.15.
 */
public class ApiService extends IntentService {

    private static final String APPLICATION_NAME = "Google";

    private static final String API_KEY = "AIzaSyBv-UeDfb5jh7is9j-kxL8EioZ0dswun0k";
    private static final String ENGINE_ID = "002666030648595954960:8pzmaxnty3i";
    /** Global instance of the JSON factory. */
    private static final JsonFactory JSON_FACTORY = JacksonFactory.getDefaultInstance();

    /** Global instance of the HTTP transport. */
    private static HttpTransport httpTransport;

    @SuppressWarnings("unused")
    private static Customsearch client;

    public ApiService() {this("apiService");}

    public ApiService(String name) {
        super(name);
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        Bundle args = intent.getExtras();
        int requestType = args.getInt(Const.REQUEST_TYPE);
        String requestQueryString = args.getString(Const.REQUEST_QUERY_STRING);
        long requestOffset = args.getLong(Const.REQUEST_QUERY_OFFSET);
        switch (requestType) {
            case Const.GET_IMAGES:
                try {
                    List<Result> items = getCustomSearchResults(requestQueryString, requestOffset);
                    List<ImageModelPersistant> imageModels = new ArrayList<>();
                    for(Result result : items) {
                        ImageModelPersistant imageModel = new ImageModelPersistant(result);
                        imageModels.add(imageModel);
                    }
                    ExecutorService executorService = Executors.newFixedThreadPool(5);
                    cacheModels(executorService, imageModels);
                } catch (IOException e) {
                    System.err.println(e.getMessage());
                } catch (GeneralSecurityException e) {
                    e.printStackTrace();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }

                break;
        }
    }

    private List<Result> getCustomSearchResults(String queryString, long offset) throws IOException, GeneralSecurityException {
        httpTransport = AndroidHttp.newCompatibleTransport();
        // set up global Customsearch instance
        client = new Customsearch.Builder(httpTransport, JSON_FACTORY, null)
                .setGoogleClientRequestInitializer(new CustomsearchRequestInitializer(API_KEY))
                .setApplicationName(APPLICATION_NAME).build();


        System.out.println("Success! Now add code here.");

        //instantiate a Customsearch.Cse.List object with your search string
        com.google.api.services.customsearch.Customsearch.Cse.List list = client.cse().list(queryString);

        //set your custom search engine id
        list.setCx(ENGINE_ID);

        // ADD searchType "image".
        list.setSearchType("image");
        //list.setStart(offset);
        //execute method returns a com.google.api.services.customsearch.model.Search object
        Search results = list.execute();

        //getItems() is a list of com.google.api.services.customsearch.model.Result objects which have the items you want
        return results.getItems();
    }

    private void cacheModels(ExecutorService executorService, List<ImageModelPersistant> imageModels) throws InterruptedException {
        for(ImageModelPersistant imageModel: imageModels) {
            executorService.execute(new DownloadTask(imageModel));
        }
        executorService.shutdown();
        executorService.awaitTermination(1, TimeUnit.SECONDS);
    }

    private class DownloadTask implements Runnable {
        private ImageModelPersistant imageModel;
        public DownloadTask(ImageModelPersistant imageModel) {
            this.imageModel = imageModel;
        }

        @Override
        public void run() {
            try {
                Bitmap bitmap = downloadBitmap(imageModel.getThumbnailLink());
                String fileName = imageModel.getThumbnailLink().substring(imageModel.getThumbnailLink().indexOf('=') + 1);
                imageModel.setThumbnailLocalPath(fileName);
                FileOutputStream fos = openFileOutput(fileName, MODE_PRIVATE);
                bitmap.compress(Bitmap.CompressFormat.JPEG, 100, fos);
                fos.close();
                ContentValues cv = imageModel.toContentValues();
                getContentResolver().insert(ImagesProvider.URI_IMAGES, cv);

            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public static Bitmap downloadBitmap(String url) throws IOException {
        HttpUriRequest request = new HttpGet(url);
        HttpClient httpClient = new DefaultHttpClient();
        HttpResponse response = httpClient.execute(request);
        StatusLine statusLine = response.getStatusLine();
        int statusCode = statusLine.getStatusCode();
        if (statusCode == 200) {
            HttpEntity entity = response.getEntity();
            byte[] bytes = EntityUtils.toByteArray(entity);

            Bitmap bitmap = BitmapFactory.decodeByteArray(bytes, 0,
                    bytes.length);
            return bitmap;
        } else {
            throw new IOException("Download failed, HTTP response code "
                    + statusCode + " - " + statusLine.getReasonPhrase());
        }
    }
}
