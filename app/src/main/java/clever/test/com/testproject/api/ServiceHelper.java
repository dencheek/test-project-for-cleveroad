package clever.test.com.testproject.api;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;

import clever.test.com.testproject.Const;

/**
 * Created by user on 19.02.15.
 */
public class ServiceHelper {

    private Context context;

    public ServiceHelper(Context context) {
        this.context = context;
    }

    public void getImages(String queryString, long offset) {
        Intent i = new Intent(context, ApiService.class);
        Bundle bundle = new Bundle();
        bundle.putInt(Const.REQUEST_TYPE, Const.GET_IMAGES);
        bundle.putString(Const.REQUEST_QUERY_STRING, queryString);
        bundle.putLong(Const.REQUEST_QUERY_OFFSET, offset);
        i.putExtras(bundle);
        context.startService(i);
    }


}










