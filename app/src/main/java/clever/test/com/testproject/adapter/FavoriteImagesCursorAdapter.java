package clever.test.com.testproject.adapter;

import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.view.View;
import clever.test.com.testproject.R;
import clever.test.com.testproject.fragments.ImageFragment;
import clever.test.com.testproject.model.FavoriteModelPersistant;
import clever.test.com.testproject.model.ImageModelPersistant;
import clever.test.com.testproject.utils.CommonUtils;

/**
 * Created by user on 20.02.15.
 */
public class FavoriteImagesCursorAdapter extends BaseImagesCursorAdapter {

    public FavoriteImagesCursorAdapter(Context context) {
        super(context);
    }

    @Override
    protected void onViewBinded(View view, ViewHolder viewHolder, final Context context, Cursor cursor) {
        viewHolder.chbIsCached.setVisibility(View.GONE);
        final FavoriteModelPersistant favoriteModel = new FavoriteModelPersistant(cursor);
        viewHolder.ivThumbnail.setImageBitmap(CommonUtils.decodeBitmapFromFile(context, favoriteModel.getThumbnailLocalPath()));
        viewHolder.tvTitle.setText(favoriteModel.getTitle());
        view.setTag(R.id.image_model, favoriteModel);
        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FavoriteModelPersistant favoriteModel = (FavoriteModelPersistant) v.getTag(R.id.image_model);
                ImageFragment imageFragment = new ImageFragment();
                Bundle bundle = new Bundle();
                bundle.putString("action", "fromFile");
                bundle.putString("expImageUrl", favoriteModel.getLocalPath());
                imageFragment.setArguments(bundle);
                fragmentManager.beginTransaction().replace(R.id.fragmentContainer, imageFragment).addToBackStack(null).commit();
            }
        });
    }
}
