package clever.test.com.testproject.fragments;

import android.database.Cursor;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import clever.test.com.testproject.R;
import clever.test.com.testproject.adapter.FavoriteImagesCursorAdapter;
import clever.test.com.testproject.provider.FavoritesProvider;

/**
 * Created by user on 20.02.15.
 */
public class FavoriteImagesFragment extends Fragment implements LoaderManager.LoaderCallbacks<Cursor> {

    private FavoriteImagesCursorAdapter adapter;
    private ListView lvImagesList;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,  Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_search_images, container, false);
        lvImagesList = (ListView) view.findViewById(R.id.fragment_search_images_lv_items);
        lvImagesList.setFocusable(false);
        adapter = new FavoriteImagesCursorAdapter(getActivity());
        lvImagesList.setAdapter(adapter);
        getLoaderManager().initLoader(1, null, this);
        return view;
    }

    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        return new CursorLoader(getActivity(), FavoritesProvider.URI_IMAGES, null, null, null, null);
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor cursor) {
        adapter.swapCursor(cursor);
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {
        adapter.swapCursor(null);
    }
}
