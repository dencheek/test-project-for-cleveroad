package clever.test.com.testproject.utils;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

/**
 * Created by user on 20.02.15.
 */
public class CommonUtils {
    public static Bitmap decodeBitmapFromFile(Context context, String filePath) {
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inPreferredConfig = Bitmap.Config.ARGB_8888;
        return BitmapFactory.decodeFile(context.getFilesDir() + "/" + filePath, options);
    }
}
