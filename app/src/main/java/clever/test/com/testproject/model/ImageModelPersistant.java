package clever.test.com.testproject.model;

import android.content.ContentValues;
import android.database.Cursor;

import com.google.api.services.customsearch.model.Result;

import clever.test.com.testproject.provider.ImagesProvider;

/**
 * Created by user on 19.02.15.
 */
public class ImageModelPersistant {

    private int id;
    private String title;
    private String thumbnailLink;
    private String link;
    private String thumbnailLocalPath;
    private boolean isCached;

    public ImageModelPersistant(Result searchResult) {
        title = searchResult.getTitle();
        thumbnailLink = searchResult.getImage().getThumbnailLink();
        link = searchResult.getLink();
    }

    public ImageModelPersistant(Cursor cursor) {
        id = cursor.getInt(cursor.getColumnIndex(ImagesProvider.ID));
        title = cursor.getString(cursor.getColumnIndex(ImagesProvider.IMAGE_TITLE));
        link = cursor.getString(cursor.getColumnIndex(ImagesProvider.IMAGE_LINK));
        isCached = cursor.getInt(cursor.getColumnIndex(ImagesProvider.IS_CACHED)) == 1;
        thumbnailLocalPath = cursor.getString(cursor.getColumnIndex(ImagesProvider.IMAGE_THUMBNAIL_LOCAL_PATH));
    }

    public ContentValues toContentValues() {
        ContentValues cv = new ContentValues();
        cv.put(ImagesProvider.IMAGE_TITLE, title);
        cv.put(ImagesProvider.IMAGE_LINK, link);
        cv.put(ImagesProvider.IMAGE_THUMBNAIL_LOCAL_PATH, thumbnailLocalPath);
        cv.put(ImagesProvider.IS_CACHED, isCached ? 1 : 0);
        return cv;
    }

    public String getTitle() {
        return title;
    }

    public String getThumbnailLink() {
        return thumbnailLink;
    }

    public String getLink() {
        return link;
    }

    public void setThumbnailLocalPath(String thumbnailLocalPath) {
        this.thumbnailLocalPath = thumbnailLocalPath;
    }

    public String getThumbnailLocalPath() {
        return thumbnailLocalPath;
    }

    public boolean isCached() {
        return isCached;
    }

    public void setCached(boolean isCached) {
        this.isCached = isCached;
    }

    public int getId() {
        return id;
    }
}
