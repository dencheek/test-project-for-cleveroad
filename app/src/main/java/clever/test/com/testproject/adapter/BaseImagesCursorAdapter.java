package clever.test.com.testproject.adapter;

import android.content.Context;
import android.database.Cursor;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CursorAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import clever.test.com.testproject.R;

/**
 * Created by user on 20.02.15.
 */
public abstract class BaseImagesCursorAdapter extends CursorAdapter {

    protected FragmentManager fragmentManager;
    public BaseImagesCursorAdapter(Context context) {
        super(context, null, false);
        fragmentManager = ((FragmentActivity)context).getSupportFragmentManager();

    }

    @Override
    public View newView(Context context, Cursor cursor, ViewGroup parent) {
        View v =  LayoutInflater.from(context).inflate(R.layout.item_list, parent, false);
        ImagesCursorAdapter.ViewHolder viewHolder = new ImagesCursorAdapter.ViewHolder();
        viewHolder.chbIsCached = (CheckBox) v.findViewById(R.id.item_list_chb_is_cached);
        viewHolder.ivThumbnail = (ImageView) v.findViewById(R.id.item_list_iv_image);
        viewHolder.tvTitle = (TextView) v.findViewById(R.id.item_list_tv_title);
        v.setTag(R.id.view_holder, viewHolder);
        return v;
    }

    protected abstract void onViewBinded(View view, ViewHolder viewHolder, Context context, Cursor cursor);

    @Override
    public void bindView(View view, Context context, Cursor cursor) {
        BaseImagesCursorAdapter.ViewHolder viewHolder = (BaseImagesCursorAdapter.ViewHolder) view.getTag(R.id.view_holder);
        viewHolder.ivThumbnail = (ImageView) view.findViewById(R.id.item_list_iv_image);
        viewHolder.tvTitle = (TextView) view.findViewById(R.id.item_list_tv_title);
        viewHolder.chbIsCached = (CheckBox) view.findViewById(R.id.item_list_chb_is_cached);
        onViewBinded(view, viewHolder, context, cursor);
    }

    protected static class ViewHolder {
        public ImageView ivThumbnail;
        public CheckBox chbIsCached;
        public TextView tvTitle;
    }
}
