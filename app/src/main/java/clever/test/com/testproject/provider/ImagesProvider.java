package clever.test.com.testproject.provider;

import android.content.ContentProvider;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Context;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.net.Uri;

import java.sql.SQLException;

/**
 * Created by user on 19.02.15.
 */
public class ImagesProvider extends ContentProvider {

    private static final String DB_NAME = "imagesdb";
    private static final int DB_VERSION = 14;
    public static final String TABLE_IMAGES = "images";

    public static final String ID = "_id";
    public static final String IMAGE_TITLE = "image_title";
    public static final String IMAGE_LINK = "image_link";
    public static final String IMAGE_THUMBNAIL_LOCAL_PATH = "image_tumbnail_local_path";
    public static final String IS_CACHED = "is_cached";

    private final static String CREATE_TABLE = "create table " + TABLE_IMAGES + " ("
            + ID + " integer primary key, "
            + IMAGE_TITLE + " text, "
            + IMAGE_LINK + " text, "
            + IMAGE_THUMBNAIL_LOCAL_PATH + " text, "
            + IS_CACHED + " integer default 0"
            + ");";

    public final static String AUTHORITY = "com.testproject.provider.ImagesProvider";

    public final static Uri URI_IMAGES = Uri.parse("content://" + AUTHORITY + "/" + TABLE_IMAGES);

    private final static String DROP_TABLE = "drop table if exists " + TABLE_IMAGES;

    private final static int MATCH_IMAGES_ITEM = 0;
    private final static int MATCH_IMAGES_DIR = 1;

    private UriMatcher URI_MATCHER = new UriMatcher(UriMatcher.NO_MATCH) {{
        addURI(AUTHORITY, TABLE_IMAGES + "/#", MATCH_IMAGES_ITEM);
        addURI(AUTHORITY, TABLE_IMAGES, MATCH_IMAGES_DIR);
    }};

    private DbOpenHelper dbOpenHelper;
    private SQLiteDatabase db;

    @Override
    public boolean onCreate() {
        dbOpenHelper = new DbOpenHelper(getContext());
        return true;
    }

    @Override
    public Cursor query(Uri uri, String[] projection, String selection, String[] selectionArgs, String sortOrder) {
        switch (URI_MATCHER.match(uri)) {
            case MATCH_IMAGES_DIR:
                break;
            case MATCH_IMAGES_ITEM:
                selection = ID + "=" + uri.getLastPathSegment();
        }
        db = dbOpenHelper.getReadableDatabase();
        Cursor cursor = db.query(TABLE_IMAGES, projection, selection, selectionArgs, null, null, sortOrder);
        cursor.setNotificationUri(getContext().getContentResolver(), uri);
        return cursor;
    }

    @Override
    public String getType(Uri uri) {
        return null;
    }

    @Override
    public Uri insert(Uri uri, ContentValues values) {
        switch (URI_MATCHER.match(uri)) {
            case MATCH_IMAGES_ITEM:
                throw new IllegalArgumentException("Wrong Uri: " + uri);
        }
        db = dbOpenHelper.getWritableDatabase();
        long rowId = db.insert(TABLE_IMAGES, null, values);
        Uri insertUri = ContentUris.withAppendedId(uri, rowId);
        getContext().getContentResolver().notifyChange(insertUri, null);
        return insertUri;
    }

    @Override
    public int delete(Uri uri, String selection, String[] selectionArgs) {
        db = dbOpenHelper.getWritableDatabase();
        int affectedRowCount = db.delete(TABLE_IMAGES, selection, selectionArgs);
        getContext().getContentResolver().notifyChange(uri, null);
        return affectedRowCount;
    }

    @Override
    public int update(Uri uri, ContentValues values, String selection, String[] selectionArgs) {
        switch (URI_MATCHER.match(uri)) {
            case MATCH_IMAGES_DIR:
                throw new IllegalArgumentException("Wrong Uri: " + uri);
            case MATCH_IMAGES_ITEM:
                selection = ID + "=" + uri.getLastPathSegment();
                break;
        }
        db = dbOpenHelper.getWritableDatabase();
        int rowsAffected = db.update(TABLE_IMAGES, values, selection, selectionArgs);
        getContext().getContentResolver().notifyChange(uri, null);
        return rowsAffected;
    }

    @Override
    public int bulkInsert(Uri uri, ContentValues[] values) {
        String table;
        switch (URI_MATCHER.match(uri)) {
            case MATCH_IMAGES_DIR:
                table = TABLE_IMAGES;
                break;
            default:
                throw new IllegalArgumentException("Wrong Uri: " + uri);
        }
        db = dbOpenHelper.getWritableDatabase();
        db.beginTransaction();

        try {
            for(ContentValues cv : values) {
                long newId = db.insertOrThrow(table, null, cv);
                if(newId <= 0) {
                    throw new SQLException("Failed to insert row into " + uri);
                }
            }
            db.setTransactionSuccessful();
            getContext().getContentResolver().notifyChange(uri, null);

        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            db.endTransaction();
        }
        return values.length;
    }

    private class DbOpenHelper extends SQLiteOpenHelper {

        public DbOpenHelper(Context context) {
            super(context, DB_NAME, null, DB_VERSION);
        }

        @Override
        public void onCreate(SQLiteDatabase db) {
            db.execSQL(CREATE_TABLE);
        }

        @Override
        public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
            db.execSQL(DROP_TABLE);
            onCreate(db);
        }
    }
}
