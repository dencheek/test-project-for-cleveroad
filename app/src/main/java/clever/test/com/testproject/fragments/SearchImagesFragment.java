package clever.test.com.testproject.fragments;

import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.database.Cursor;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.*;

import clever.test.com.testproject.ApplicationManager;
import clever.test.com.testproject.Const;
import clever.test.com.testproject.R;
import clever.test.com.testproject.adapter.ImagesCursorAdapter;
import clever.test.com.testproject.api.ServiceHelper;
import clever.test.com.testproject.provider.ImagesProvider;

/**
 * Created by user on 19.02.15.
 */
public class SearchImagesFragment extends Fragment implements AbsListView.OnScrollListener, LoaderManager.LoaderCallbacks<Cursor> {
    private ServiceHelper serviceHelper;
    private ImagesCursorAdapter adapter;
    private ListView lvImagesList;
    private int preLast;
    private static long offset = 0;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,  Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_search_images, container, false);
        serviceHelper = new ServiceHelper(getActivity());
        lvImagesList = (ListView) view.findViewById(R.id.fragment_search_images_lv_items);
        lvImagesList.setFocusable(false);
        adapter = new ImagesCursorAdapter(getActivity());
        lvImagesList.setAdapter(adapter);
        lvImagesList.setOnScrollListener(this);
        getLoaderManager().initLoader(0, null, this);
        return view;
    }

    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        return new CursorLoader(getActivity(), ImagesProvider.URI_IMAGES, null, null, null, null);
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor cursor) {
        adapter.swapCursor(cursor);
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {
        adapter.swapCursor(null);
    }

    @Override
    public void onScrollStateChanged(AbsListView view, int scrollState) {

    }

    @Override
    public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
        final int lastItem = firstVisibleItem + visibleItemCount;
        if(preLast != lastItem && offset <= 20) {
            preLast = lastItem;
            offset += 10;
            if(ApplicationManager.currentQuery != null)
                serviceHelper.getImages(ApplicationManager.currentQuery, offset );
        }
    }
}
