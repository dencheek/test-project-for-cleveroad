package clever.test.com.testproject.adapter;

import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.view.View;
import android.widget.CheckBox;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

import clever.test.com.testproject.Const;
import clever.test.com.testproject.R;
import clever.test.com.testproject.api.ApiService;
import clever.test.com.testproject.fragments.ImageFragment;
import clever.test.com.testproject.model.FavoriteModelPersistant;
import clever.test.com.testproject.model.ImageModelPersistant;
import clever.test.com.testproject.provider.FavoritesProvider;
import clever.test.com.testproject.provider.ImagesProvider;
import clever.test.com.testproject.utils.CommonUtils;

/**
 * Created by user on 19.02.15.
 */
public class ImagesCursorAdapter extends BaseImagesCursorAdapter {

    public ImagesCursorAdapter(Context context) {
        super(context);
    }

    @Override
    protected void onViewBinded(View view, ViewHolder viewHolder, final Context context, Cursor cursor) {

        final ImageModelPersistant imageModel = new ImageModelPersistant(cursor);
        viewHolder.chbIsCached.setChecked(imageModel.isCached());
        viewHolder.chbIsCached.setTag(imageModel);
        viewHolder.chbIsCached.setOnCheckedChangeListener(null);
        viewHolder.chbIsCached.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CheckBox chbIsCached = (CheckBox) v;
                ImageModelPersistant imageModel = (ImageModelPersistant) v.getTag();
                if(chbIsCached.isChecked()) {
                    new CacheImageTask(context, imageModel).execute();
                } else {
                    imageModel.setCached(false);
                    context.getContentResolver().delete(FavoritesProvider.URI_IMAGES,
                            FavoritesProvider.ID + "=" + imageModel.getId(), null);
                    context.getContentResolver().update(ContentUris.withAppendedId(ImagesProvider.URI_IMAGES, imageModel.getId()), imageModel.toContentValues(), null, null);
                }
            }
        });

        viewHolder.ivThumbnail.setImageBitmap(CommonUtils.decodeBitmapFromFile(context, imageModel.getThumbnailLocalPath()));
        viewHolder.tvTitle.setText(imageModel.getTitle());
        view.setTag(R.id.image_model, imageModel);
        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ImageModelPersistant imageModel = (ImageModelPersistant) v.getTag(R.id.image_model);
                ImageFragment imageFragment = new ImageFragment();
                Bundle bundle = new Bundle();
                bundle.putString(Const.Extras.ACTION, Const.Action.DOWNLOAD_FROM_API);
                bundle.putString(Const.Extras.EXP_IMAGE_URL, imageModel.getLink());
                imageFragment.setArguments(bundle);
                fragmentManager.beginTransaction().replace(R.id.fragmentContainer, imageFragment).addToBackStack(null).commit();
            }
        });
    }

    private class CacheImageTask extends AsyncTask<Void, Void, Void> {

        private ImageModelPersistant imageModel;
        private Context context;
        public CacheImageTask(Context context, ImageModelPersistant imageModel) {
            this.imageModel = imageModel;
            this.context = context;
        }

        @Override
        protected Void doInBackground(Void... params) {

            FileOutputStream fos = null;
            try {
                Bitmap bitmap = ApiService.downloadBitmap(imageModel.getLink());
                String localPath = /*"images/" + */Uri.parse(imageModel.getLink()).getLastPathSegment().toString();
                fos = context.openFileOutput(localPath, Context.MODE_PRIVATE);
                bitmap.compress(Bitmap.CompressFormat.JPEG, 100, fos);
                FavoriteModelPersistant favoritesModel = new FavoriteModelPersistant(imageModel, localPath);
                ContentValues cv = favoritesModel.toContentValues();
                context.getContentResolver().insert(FavoritesProvider.URI_IMAGES, cv);
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            } finally {
                if(fos != null)
                    try {
                        fos.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            imageModel.setCached(true);
            context.getContentResolver().update(
                    ContentUris.withAppendedId(ImagesProvider.URI_IMAGES, imageModel.getId()),
                    imageModel.toContentValues(), null, null);
        }
    }


}
