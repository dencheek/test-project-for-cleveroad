package clever.test.com.testproject;

/**
 * Created by user on 19.02.15.
 */
public class Const {

    public static final String REQUEST_TYPE = "request_type";
    public final static int GET_IMAGES = 0;
    public static final String REQUEST_QUERY_STRING = "request_query_string";
    public static final String REQUEST_QUERY_OFFSET = "request_query_offset";

    public static final long DEFAULT_OFFSET_STEP = 10;

    public class Action {
        public static final String DOWNLOAD_FROM_API = "fromApi";
        public static final String DOWNLOAD_FROM_FILE = "fromFile";
    }

    public class Extras {
        public static final String EXP_IMAGE_URL = "expImageUrl";
        public static final String ACTION = "action";
    }
}
