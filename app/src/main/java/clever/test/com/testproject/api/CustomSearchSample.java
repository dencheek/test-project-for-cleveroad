package clever.test.com.testproject.api;

import com.google.api.client.googleapis.javanet.GoogleNetHttpTransport;
import com.google.api.client.http.HttpTransport;
import com.google.api.client.json.JsonFactory;
import com.google.api.client.json.jackson2.JacksonFactory;
import com.google.api.services.customsearch.Customsearch;
import com.google.api.services.customsearch.CustomsearchRequestInitializer;
import com.google.api.services.customsearch.model.Result;
import com.google.api.services.customsearch.model.Search;

import java.io.IOException;
import java.util.List;

public class CustomSearchSample {

    /**
     * Be sure to specify the name of your application. If the application name is {@code null} or
     * blank, the application will log a warning. Suggested format is "MyCompany-ProductName/1.0".
     */
    private static final String APPLICATION_NAME = "Google";

    // ServerIP
    private static final String API_KEY = "AIzaSyD81xLerhIBsHjsRUCau28EXqwyRWQHpeM";

    /** Global instance of the JSON factory. */
    private static final JsonFactory JSON_FACTORY = JacksonFactory.getDefaultInstance();

    /** Global instance of the HTTP transport. */
    private static HttpTransport httpTransport;

    @SuppressWarnings("unused")
    private static Customsearch client;




    public static void main(String[] args) {
        try {
            // initialize the transport
            httpTransport = GoogleNetHttpTransport.newTrustedTransport();

            // set up global Customsearch instance
            client = new Customsearch.Builder(httpTransport, JSON_FACTORY, null)
                    .setGoogleClientRequestInitializer(new CustomsearchRequestInitializer(API_KEY))
                    .setApplicationName(APPLICATION_NAME).build();


            System.out.println("Success! Now add code here.");

            //instantiate a Customsearch.Cse.List object with your search string
            com.google.api.services.customsearch.Customsearch.Cse.List list = client.cse().list("horse");

            //set your custom search engine id
            list.setCx("003043242152344800127:vaeqrm9fypm");

            // ADD searchType "image".
            list.setSearchType("image");

            //execute method returns a com.google.api.services.customsearch.model.Search object
            Search results = list.execute();

            //getItems() is a list of com.google.api.services.customsearch.model.Result objects which have the items you want
            List<Result> items = results.getItems();
            Result result = new Result();



            System.out.println("items:" + items);


        } catch (IOException e) {
            System.err.println(e.getMessage());
        } catch (Throwable t) {
            t.printStackTrace();
        }
        System.exit(1);
    }

}