package clever.test.com.testproject.fragments;

import android.app.ProgressDialog;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import java.io.IOException;

import clever.test.com.testproject.Const;
import clever.test.com.testproject.R;
import clever.test.com.testproject.api.ApiService;
import clever.test.com.testproject.utils.CommonUtils;
import clever.test.com.testproject.utils.ZoomDetector;

/**
 * Created by user on 20.02.15.
 */
public class ImageFragment extends Fragment {

    ImageView ivExpanded;

    private class ImageDownloadTask extends AsyncTask<String, Void, Bitmap> {

        private ProgressDialog progressDialog;
        private String action;
        public ImageDownloadTask(String action) {
            this.action = action;
        }

        @Override
        protected void onPreExecute() {
            progressDialog = new ProgressDialog(getActivity());
            progressDialog.setMessage("Please, wait!");
            progressDialog.show();
        }

        @Override
        protected Bitmap doInBackground(String... params) {
            String uri = params[0];
            Bitmap bitmap = null;
            try {
                if(Const.Action.DOWNLOAD_FROM_API.equals(action)) {
                    bitmap = ApiService.downloadBitmap(uri);
                } else if(Const.Action.DOWNLOAD_FROM_FILE.equals(action)) {
                    bitmap = CommonUtils.decodeBitmapFromFile(getActivity(), uri);
                }

            } catch (IOException e) {
                e.printStackTrace();
            }
            return bitmap;
        }

        @Override
        protected void onPostExecute(Bitmap bitmap) {
            if(progressDialog.isShowing())
                progressDialog.dismiss();
            ivExpanded.setImageBitmap(bitmap);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_image, container, false);
        getActivity().getActionBar().hide();
        setRetainInstance(true);
        ivExpanded = (ImageView) view.findViewById(R.id.fragment_image_iv_expanded);
        ivExpanded.setOnTouchListener(new ZoomDetector());
        String action = (String) getArguments().get(Const.Extras.ACTION);
        String uri = (String) getArguments().get(Const.Extras.EXP_IMAGE_URL);
        new ImageDownloadTask(action).execute(uri);
        return view;
    }

    @Override
    public void onDestroy() {
        getActivity().getActionBar().show();
        super.onDestroy();
    }
}
